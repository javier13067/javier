class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
    before_action :authenticate_user!
    before_action :configure_permitted_parameters, if: :devise_controller?
    
    protected
    
    def ofter_sign_in_path_for(resourse)
        if current_admin
            users_list
        else
            zombies_path
        end
    end
    
    
    
    def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:role])
    end
    
end
